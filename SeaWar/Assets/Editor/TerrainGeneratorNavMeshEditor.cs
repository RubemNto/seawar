using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TerrainGeneratorNavMesh))]
public class TerrainGeneratorNavMeshEditor : Editor
{
    public override void OnInspectorGUI()
    {
	base.OnInspectorGUI();
	TerrainGeneratorNavMesh tg = (TerrainGeneratorNavMesh)target;
	if(GUILayout.Button("Generate Nav Mesh")){
	    tg.GenerateWalkableVertices();
	}
    }
}
