using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TerrainGenerator))]
public class TerrainGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
	base.OnInspectorGUI();
	TerrainGenerator tg = (TerrainGenerator)target;
	if(GUILayout.Button("Generate Terrain")){
	    tg.GenerateTerrain();
	}
    }
}
