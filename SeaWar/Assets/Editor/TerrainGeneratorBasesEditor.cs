using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TerrainGeneratorBases))]
public class TerrainGeneratorBasesEditor : Editor
{
    public override void OnInspectorGUI()
    {
	base.OnInspectorGUI();
	TerrainGeneratorBases tgb = (TerrainGeneratorBases)target;
	if(GUILayout.Button("Position Bases")){
	    tgb.PositionBases();
	}
    }

}
