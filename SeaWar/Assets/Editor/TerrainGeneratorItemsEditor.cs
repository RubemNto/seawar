using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TerrainGeneratorItems))]
public class TerrainGeneratorItemsEditor : Editor
{
    public override void OnInspectorGUI()
    {
	base.OnInspectorGUI();
	TerrainGeneratorItems tg = (TerrainGeneratorItems)target;
	if(GUILayout.Button("Place Items")){
	    tg.DestroyInstancedItems();
	    tg.GenerateRandomItemTileArray(); 
	    tg.PlaceItems();
	}
    }
}
