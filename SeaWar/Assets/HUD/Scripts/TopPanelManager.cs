using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TopPanelManager : MonoBehaviour
{
    public TextMeshProUGUI coralCountText; 
    public TextMeshProUGUI rockCountText; 
    public TextMeshProUGUI energyRockCountText; 
    public TextMeshProUGUI vidraniumCountText; 

    public BaseManager baseManager;
    
    // Update is called once per frame
    void Update()
    {
	coralCountText.text      =baseManager.coralCount.ToString() ;      	
	rockCountText.text       =baseManager.rockCount.ToString() ;
	energyRockCountText.text =baseManager.energyRockCount.ToString() ;
	vidraniumCountText.text  =baseManager.vidraniumCount.ToString() ;
    }
}
