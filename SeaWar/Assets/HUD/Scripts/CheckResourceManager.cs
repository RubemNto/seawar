using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CheckResourceManager : MonoBehaviour
{
    public BaseManager baseManager; 

    public TextMeshProUGUI countTextRock;
    public TextMeshProUGUI countTextCoral;
    public TextMeshProUGUI countTextVidranium;

    public Button targetButton;

    public GameObject camera;
    
    private bool HasResources(){
	int cost1 = int.Parse(countTextRock.text);
	if(cost1 > baseManager.rockCount){return false;}

	int cost2 = int.Parse(countTextCoral.text);
	if(cost2 > baseManager.coralCount){return false;}

	int cost3 = int.Parse(countTextVidranium.text);
	if(cost1 > baseManager.vidraniumCount){return false;}
	return true;
    }

    public void ChargeResources(){
	int cost1 = int.Parse(countTextRock.text);
	baseManager.rockCount -= cost1;

	int cost2 = int.Parse(countTextCoral.text);
	baseManager.coralCount -= cost2;

	int cost3 = int.Parse(countTextVidranium.text);
	baseManager.vidraniumCount -= cost3;
    }

    public void InstantiateModel()
    {
	GameObject model = transform.parent.gameObject.GetComponent<ContentPanelManagerBuild>().buildData.buildModel;
	CameraRayPositionTarget crpt = Camera.main.GetComponent<CameraRayPositionTarget>();
	model = Instantiate(model, crpt.cameraRay.hitPoint,Quaternion.identity) as GameObject;
	crpt.target = model;
    }

    private void Update()
    {
	targetButton.interactable = HasResources();
    }
}
