using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextPanelManager : MonoBehaviour
{
    bool currentState;
    public BaseManager baseManager;
    public Animator animator;

    void Start(){
	currentState = baseManager.inCraftState; 
    }
    
    void Update()
    {
	if(currentState != baseManager.inCraftState)
	{
	    if(baseManager.inCraftState)
	    {
		animator.SetTrigger("Open");
	    }else
	    {
		animator.SetTrigger("Close");
	    }
	    currentState = baseManager.inCraftState; 
	}
    }
}
