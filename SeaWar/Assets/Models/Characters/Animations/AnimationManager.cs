using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public Animator animator;
    
    public void SetAttack(bool state){
	animator.SetBool("Collect",false);
	animator.SetBool("Move",false);
	animator.SetBool("Mine",false);
	animator.SetBool("Attack",state);
    }
    
    public void SetMine(bool state){
	animator.SetBool("Attack",false);
	animator.SetBool("Collect",false);
	animator.SetBool("Move",false);
	animator.SetBool("Mine",state);
    }

    public void SetMove(bool state){
	animator.SetBool("Attack",false);
	animator.SetBool("Mine",false);
	animator.SetBool("Collect",false);
	animator.SetBool("Move",state);
    }

    public void SetCollecting(bool state){
	animator.SetBool("Attack",false);
	animator.SetBool("Mine",false);
	animator.SetBool("Move",false);
	animator.SetBool("Collect",state);
    }

    public void EnableCollider(){
	GetComponent<Collider>().enabled = true;
    }
    public void DisableCollider(){
	GetComponent<Collider>().enabled = false;
    }
}
