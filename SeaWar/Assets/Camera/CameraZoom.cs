using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraZoomType{
    orthographic,
    perspective
}

public class CameraZoom : MonoBehaviour
{
    public CameraZoomType zoomType;
    public Camera mainCamera;
    [Range(1,10)]
    public float maxZoomLevel;
    [Range(1,10)]
    public float minZoomLevel;
    public float zoomSpeed;
    private float lastDistance;
    private float screenWidth;
    private Vector2 touch0d, touch1d;

    // Start is called before the first frame update
    void Start()
    {
        minZoomLevel = minZoomLevel < maxZoomLevel ? maxZoomLevel : minZoomLevel;
        mainCamera.orthographicSize = minZoomLevel; 
        lastDistance = 0;

        screenWidth = Screen.width;
        touch0d = Vector2.zero;
        touch1d = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount >= 2)
        {
            Vector2 touch0, touch1;
            touch0 = Input.GetTouch(0).position;
            touch1 = Input.GetTouch(1).position;
            float distance = Vector2.Distance(touch0, touch1);
            float distanced = Vector2.Distance(touch0d, touch1d);

            if(distance > distanced) //zoom in
            {
		if(zoomType == CameraZoomType.orthographic)
		{
		    mainCamera.orthographicSize = mainCamera.orthographicSize <= maxZoomLevel ? maxZoomLevel : mainCamera.orthographicSize - zoomSpeed*Time.deltaTime;
		}else
		{
		    if(mainCamera.transform.position.y > 10.6f)
		    {
			mainCamera.transform.position += mainCamera.transform.forward * zoomSpeed * Time.deltaTime;
		    }
		}
		
	    }else // Zoom out
            {

		if(zoomType == CameraZoomType.orthographic)
		{
		    mainCamera.orthographicSize = mainCamera.orthographicSize >= minZoomLevel ? minZoomLevel : mainCamera.orthographicSize + zoomSpeed*Time.deltaTime;
		}else
		{
		    if(mainCamera.transform.position.y <= 25.0f)
		    {
			mainCamera.transform.position -= mainCamera.transform.forward * zoomSpeed * Time.deltaTime;
		    }
		}
            }
            touch0d = touch0;
            touch1d = touch1;
        }
    }
}
