using UnityEngine;

public class CameraDrag : MonoBehaviour
{
    public float dragSpeed = 2;
    private Vector3 dragOrigin;
    public bool invert;
    public float dragThreshold;
    public static CameraDrag singleton;

    void Awake()
    {
        if (singleton == null)
        {
            singleton = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void MoveCamera(Vector3 pos)
    {
        transform.position = pos + new Vector3(7.5f, 0f, -7.5f);
        transform.position = new Vector3(transform.position.x, 10.6f, transform.position.z);
        //Broadcaster.instance.BroadcastMessage("Move Camera");
    }

    void Update()
    {
        if (Input.touchCount < 2)
        {
            if (Input.GetMouseButtonDown(0))
            {
                dragOrigin = Input.mousePosition;
                return;
            }

            if (!Input.GetMouseButton(0))
                return;

            Vector3 pos = invert
                ? Camera.main.ScreenToViewportPoint(dragOrigin - Input.mousePosition)
                : Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);

            Vector3 up = Vector3.up;
            Vector3 right = transform.right;
            Vector3 forward = Vector3.Cross(up, right);

            Vector3 offset = new Vector3(pos.x * dragSpeed, 0, pos.y * dragSpeed);

            Vector3 move = (offset.x * -right + offset.z * forward) * Time.deltaTime;

            if (move.magnitude > dragThreshold)
            {
                transform.Translate(move, Space.World);
                //Broadcaster.instance.BroadcastMessage("Drag Camera");
            }
        }
    }
}
