using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRayPositionTarget : MonoBehaviour
{
    public CameraRay cameraRay;
    public GameObject target;

    private void Update(){

	if(target != null)
	{
	    target.transform.position = cameraRay.hitPoint;
	    target.transform.up = cameraRay.hitPointNormal;
	}
    }

    public void DropTarget(){
	var bm = target.GetComponent<BuildingManager>();
	bm.EnableCollider();
	target = null;
    }
}
