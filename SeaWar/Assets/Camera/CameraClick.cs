using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CameraClickData
{
    public GameObject clickedGameObject;
    public Vector3 clickedPosition;
}

public class CameraClick : MonoBehaviour
{
    public LayerMask clickableLayers;
    public GameObject indicator;
    public Vector3 indicatorOffset;

    public Vector3 target;
    public GameObject lastClicked;
    public GameObject clicked;

    private CameraClickData lastClick;
    private CameraClickData click;
    
    public CameraClickData LastClick => lastClick;
    public CameraClickData Click => click;

    public BaseManager baseManager;

    private void Update(){

	if(Input.GetKeyDown(KeyCode.Mouse0))
	{
	    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	    RaycastHit hit;
	    Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
            if (Physics.Raycast(ray, out hit,Mathf.Infinity,clickableLayers))
            {
		if (hit.transform.tag == "UI"){return;}
		if (baseManager.inCraftState == true){return;}
		
		if(hit.transform.gameObject.GetComponent<TerrainGenerator>() != null){
		    target = hit.point;
		}else{
		    target = hit.transform.position;
		}
		lastClicked = clicked;
		clicked = hit.transform.gameObject;

		lastClick = click;
		click = new CameraClickData();
		click.clickedPosition = target;
		click.clickedGameObject = clicked;
		
		//Broadcaster.BroadcastMessage(clicked.tag);
		
		Instantiate(indicator, target + indicatorOffset,indicator.transform.rotation);

		Action action = null;
		if(clicked.tag == "Terrain")
		{
		    action = new Action(clicked.tag,clicked.tag,null,target);
		}else
		{
		    action = new Action(clicked.tag,
					clicked.tag,
					clicked,
					target);
		}

		
		if(baseManager.activePawns.Contains(clicked)){
		    baseManager.activePawns.Remove(clicked);
		}
		
		foreach(var pawn in baseManager.activePawns){
		    Pawn pawnComponent = pawn.GetComponent<Pawn>();
		    pawnComponent.actionManager.AddAction(action);
		}
		
		if(baseManager.instancedPawns.Contains(clicked))
		{
		    baseManager.activePawns.Clear();
		    baseManager.activePawns.Add(clicked);
		}
		
	    }
	}
    }
    
}
