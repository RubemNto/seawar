using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRay : MonoBehaviour
{
    public GameObject hit;
    public Vector3 hitPoint;
    public Vector3 hitPointNormal;
    public bool detect;

    public void FixedUpdate(){
	if(detect == false){return;}

	RaycastHit rayHit;

	if(Physics.Raycast(transform.position,transform.forward,out rayHit, Mathf.Infinity))
	{
	    hit = rayHit.transform.gameObject;
	    hitPoint = rayHit.point;
	    hitPointNormal = rayHit.normal;
	}
    }
}
