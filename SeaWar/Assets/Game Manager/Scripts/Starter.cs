using UnityEngine;
using UnityEngine.Events;

public class Starter : MonoBehaviour
{
	public UnityEvent events; 
   
	void Start()
	{
		events.Invoke();
	}
}
