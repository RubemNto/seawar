using UnityEngine;
using UnityEngine.Events;

public class Awaker : MonoBehaviour
{
	public UnityEvent events; 
   
	void Awake()
	{
		events.Invoke();
	}
}
