using UnityEngine;
using UnityEngine.Events;

public class Updater : MonoBehaviour
{
    public UnityEvent events;
    
    // Update is called once per frame
    void Update()
    {
	events.Invoke();
    }
}
