using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGeneratorPawn : MonoBehaviour
{
    public float radius;
    public TerrainGenerator tg;
    public bool removeOnUpdate;

    public void Start(){
	tg = TerrainGenerator.singleton;
    }
    
    private void LateUpdate()
    {
	int vertexIndex = 0;
	foreach(var vertex in tg.Vertices)
	{
	    float distance = Vector2.Distance(
		new Vector2(transform.position.x,transform.position.z),
		new Vector2(vertex.x,vertex.z));

	    if(distance <= radius){
		float sample = 1 - (distance/radius);
		Color color = new Color(sample,sample,sample);

		if(tg.verticesColors[vertexIndex].grayscale < color.grayscale)
		{
		    tg.verticesColors[vertexIndex] = color;
		}
	    }

	    vertexIndex+=1;
	}
	tg.UpdateMeshVerticesColors();
	if(removeOnUpdate){Destroy(this);}
    }
    
}
