Shader "Custom/Terrain"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _SecondaryTex ("SecondaryTex (RGB)", 2D) = "white" {}

	_MainTexNormal ("Albedo Normal (RGB)", 2D) = "white" {}
        _SecondaryTexNormal ("SecondaryTex Normal (RGB)", 2D) = "white" {}

	_NormalThreshold("Normal threshold", Range(0,1)) = 0
	_DissolveLevel("Dissolve Level", Range(0,10)) = 0
	_DissolveLimit("Dissolve Limit", Range(0,10)) = 0

        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _SecondaryTex;

	sampler2D _MainTexNormal;
        sampler2D _SecondaryTexNormal;

	struct Input
        {
            float2 uv_MainTex;
	    float3 worldNormal;
	    float3 worldPos;
	    float3 viewDir;
	    float4 color : Color;
        };

        half _Glossiness;
        half _Metallic;

	float _NormalThreshold;
	float _DissolveLevel;
	float _DissolveLimit;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
	    fixed3 normal = IN.worldNormal;

            fixed4 main = tex2D (_MainTex, IN.uv_MainTex);
            fixed4 second = tex2D (_SecondaryTex, IN.uv_MainTex);

	    fixed4 mainNormal = tex2D (_MainTexNormal, IN.uv_MainTex);
            fixed4 secondNormal = tex2D (_SecondaryTexNormal, IN.uv_MainTex);

	    o.Albedo = lerp(second.rgb,main.rgb,clamp( _DissolveLimit * pow(_NormalThreshold * dot(normal,fixed3(0,1,0)),_DissolveLevel) ,0,1) );
	    o.Albedo *= IN.color.rgb;
	
            // Albedo comes from a texture tinted by color
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = main.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
