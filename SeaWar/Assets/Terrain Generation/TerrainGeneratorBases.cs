using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGeneratorBases : MonoBehaviour
{
    public GameObject base1; 
    public GameObject base2; 
    public TerrainGenerator tg;
    
    public void PositionBase1(Vector3 position,Vector3 forward){
	base1.transform.forward = forward;
	Instantiate(base1, position, base1.transform.rotation);
    }

    public void PositionBase2(Vector3 position,Vector3 forward){
	base2.transform.forward = forward;
	Instantiate(base2, position, base2.transform.rotation);
    }

    public void PositionBases(){
	var tile1 = tg.Tiles[0];
	var tile2 = tg.Tiles[tg.Tiles.Count-1];
	var pos1 = tg.Vertices[tile1[Random.Range(0,tg.tileVertexDensity)]];
	var pos2 = tg.Vertices[tile2[Random.Range(0,tg.tileVertexDensity)]];
	PositionBase1(pos1,(pos2-pos1).normalized);
	PositionBase2(pos2, (pos1-pos2).normalized);
    }
}
