using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TerrainGeneratorItems : MonoBehaviour
{
    public GameObject[] items;
    public int[] itemsCount;
    
    public int[] itemsTile;
    private TerrainGenerator terrainGenerator;

    private List<GameObject> instances;

    public Dictionary<int,List<int>> connectionRules;

    public void GenerateRuleGraph(){
	connectionRules = new Dictionary<int,List<int>>();

	for(int i = 0; i <= items.Length; i++)
	{
	    List<int> connections = new List<int>();
	    connections.Add(i);
	    connections.Add(i+1);
	    connections.Add(i+2);

	    if(i-1 >= 0){connections.Add(i-1);}

	    connectionRules.Add(i,connections);
	}
    }

    public void GenerateRuledItemsTileMap(){
	GenerateRuleGraph();

	List<int> visitedNodes = new List<int>();
	List<int> toVisitNodes = new List<int>();

	int tileCountLenght = terrainGenerator.tileCountLenght;
	int tileCountWidth = terrainGenerator.tileCountWidth;
	
	itemsTile = Enumerable.Repeat(-1,tileCountWidth * tileCountLenght).ToArray();

	for(int i = 0; i< itemsTile.Length; i++){
	    toVisitNodes.Add(i);
	}
	
	itemsTile[0] = Random.Range(0,items.Length);
	itemsTile[tileCountLenght-1] = Random.Range(0,items.Length);
	itemsTile[tileCountLenght*tileCountWidth - tileCountLenght] = Random.Range(0,items.Length);
	itemsTile[tileCountLenght*tileCountWidth-1] = Random.Range(0,items.Length);

	visitedNodes.Add(0);
	visitedNodes.Add(tileCountLenght-1);
	visitedNodes.Add(tileCountLenght*tileCountWidth - tileCountLenght);
	visitedNodes.Add(tileCountLenght*tileCountWidth-1);

	toVisitNodes.Remove(0);
	toVisitNodes.Remove(tileCountLenght-1);
	toVisitNodes.Remove(tileCountLenght*tileCountWidth - tileCountLenght);
	toVisitNodes.Remove(tileCountLenght*tileCountWidth-1);
	
	int currentNode = toVisitNodes[Random.Range(0,toVisitNodes.Count)];

	while(toVisitNodes.Count > 0)
	{
	    int[] neighbors = GetItemsMapNeighbors(currentNode);
	    List<int> availableNeighbors = new List<int>();
	    foreach(var neighbor in neighbors)
	    {
		//if(visitedNodes.Contains(neighbor) == false){
		    availableNeighbors.Add(neighbor);
		//}
	    }
	    List<int> neighborsValues = new List<int>();

	    foreach(var neighbor in availableNeighbors)
	    {
		neighborsValues.Add(itemsTile[neighbor]);
	    }
	    int[] availableValues = GetAvailableValues(neighborsValues.ToArray());

	    List<int> availableValuesList = new List<int>(availableValues);
	    int chosenValue = -1;
	    while(availableValuesList.Count > 0){
		int tempChosenValue = availableValuesList[Random.Range(0,availableValuesList.Count)];
		if(itemsCount[tempChosenValue] > 0){
		    itemsCount[tempChosenValue] -= 1;
		    chosenValue = tempChosenValue;
		    break;
		}else{
		    availableValuesList.Remove(tempChosenValue);
		}
	    }

	    itemsTile[currentNode] = chosenValue;//availableValues[Random.Range(0,availableValues.Length)];

	    toVisitNodes.Remove(currentNode);
	    visitedNodes.Add(currentNode);
	    if(toVisitNodes.Count > 0)
	    {
		currentNode = toVisitNodes[Random.Range(0,toVisitNodes.Count)];
	    }
	}	
    }

    public int[] GetItemsMapNeighbors(int i){
	if(i < 0 || i >= itemsTile.Length){return null;}

	int tileCountLenght = terrainGenerator.tileCountLenght;
	int tileCountWidth = terrainGenerator.tileCountWidth;
	
	int[] neighbors = new int[]{
	    i-tileCountLenght+1,i-tileCountLenght,i-tileCountLenght-1,
	    i+1,i-1,
	    i+tileCountLenght+1,i+tileCountLenght,i+tileCountLenght-1
	};
	
	List<int> validNeighbors = new List<int>();

	foreach(var neighbor in neighbors){
	    if(neighbor < 0 || neighbor >= itemsTile.Length){continue;}
	    if(i%tileCountLenght==0 && (i-1 == neighbor || i+tileCountLenght-1 == neighbor || i-tileCountLenght-1 == neighbor)){continue;}
	    if((i+1)%tileCountLenght==0 && (i+1 == neighbor || i+tileCountLenght+1 == neighbor || i-tileCountLenght+1 == neighbor)){continue;}
	    
	    validNeighbors.Add(neighbor);
	}
	
	return validNeighbors.ToArray();
    }
    
    public int[] GetAvailableValues(int[] input)
    {
	List<int> availableValues = new List<int>();

	for(int i = 0 ; i < items.Length;i++)
	{
	    availableValues.Add(i);
	}

	foreach(var item in input)
	{
	    if(item == -1){continue;}
	    
	    List<int> tempAvailableValues = new List<int>(availableValues.ToArray());
	    
	    var connections = connectionRules[item];

	    foreach(var value in tempAvailableValues)
	    {
		if(connections.Contains(value) == false)
		{
		    availableValues.Remove(value);
		}
	    }
	}

	return availableValues.ToArray();
    }
    
    public void DestroyInstancedItems(){
	if(instances == null){return;}
	for(int i = 0; i < instances.Count; i++){
	    DestroyImmediate(instances[i]);
	}
	instances.Clear();
    }

    public void GenerateRandomRuledItemTileArray()
    {
	for(int i = 0; i < itemsTile.Length;i++){
	    itemsTile[i] = -1;
	}
	
	Dictionary<int,int[]> rules = new Dictionary<int,int[]>();
	rules.Add(0,new int[]{0,1});
	rules.Add(1,new int[]{0,1,2});
	rules.Add(2,new int[]{1,2});
	
	int randPointsCount = Random.Range(1,itemsTile.Length/2);
    }
       

    public void GenerateRandomItemTileArray()
    {
	for(int i = 0; i < itemsTile.Length;i++){
	    itemsTile[i] = Random.Range(-items.Length,items.Length);
	}
    }
    
    public void PlaceItems()
    {
	terrainGenerator = GetComponent<TerrainGenerator>();
	terrainGenerator.GenerateData();
	
	GenerateRuledItemsTileMap();

	if(instances != null)
	DestroyInstancedItems();
	
	instances = new List<GameObject>();
	
	terrainGenerator = GetComponent<TerrainGenerator>();
	
	for(int i = 0; i < terrainGenerator.Tiles.Count; i++){
	    var tile = terrainGenerator.Tiles[i];

	    if(itemsTile[i] < 0){continue;}

	    int beginingVertex = tile[Random.Range(0,tile.Length)];
	    float maxDistanceToBeginingVertex = terrainGenerator.tileVertexDensity/2;
	    
	    foreach(var vertexIndex in tile){
		Vector3 normal = terrainGenerator.meshFilter.sharedMesh.normals[vertexIndex];
		float entropy = Vector3.Distance(
		    terrainGenerator.Vertices[vertexIndex],
		    terrainGenerator.Vertices[beginingVertex]);//Random.Range(0.0f,100.0f);
		
		if((Vector3.Dot(new Vector3(0,1,0),normal) < 0.9f)) {continue;}
		//if(entropy < 80) {continue;}
		if(entropy > maxDistanceToBeginingVertex){continue;}
		
		Vector3 position = terrainGenerator.Vertices[vertexIndex] + new Vector3(Random.Range(-0.5f,0.5f),0,Random.Range(-0.5f,0.5f));

		RaycastHit hit;
		if(Physics.Raycast(position,position + new Vector3(0,-1,0),out hit,Mathf.Infinity)){
		    position = hit.point;
		    normal = hit.normal;
		}
		
		if((Vector3.Dot(new Vector3(0,1,0),normal) < 0.9f)) {continue;}

		float randRotation = Random.Range(0.0f,360.0f);

		instances.Add(Instantiate(items[itemsTile[i]], position, Quaternion.Euler(0,0,0)));
		instances[instances.Count-1].tag = "Collectable";
		instances[instances.Count-1].transform.up = normal;
		instances[instances.Count-1].transform.Rotate(0,randRotation,0);
	    }
	}
    }
}
