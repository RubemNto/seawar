using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;

struct Node{
    int i;
    int[] neighbors;
}

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class TerrainGenerator : MonoBehaviour
{
    public float tileWidth;
    public float tileLenght;
    [Min(1)]
    public int tileVertexDensity;
    
    public int tileCountWidth;
    public int tileCountLenght;
    public float[] tilesHeightMap;

    public MeshFilter meshFilter;
    private Vector3[] vertices;
    public Color[] verticesColors;
    public Vector3[] Vertices => vertices;
    private Vector2[] uvs;
    private int[] triangles;

    public bool debug;

    public int maxHeight;

    public Dictionary<int,List<int>> connectionRules;

    [SerializeField]
    private int width,lenght;
    public int Width=>width;
    public int Lenght=>lenght;
    public List<int[]> tiles;
    public List<int[]> Tiles => tiles;

    public static TerrainGenerator singleton;

    public bool HasData(){
	return meshFilter.mesh != null && vertices != null;
    }
    
    public void GenerateData(){
	if(singleton != null){Destroy(this); return;}
	
	Mesh mesh = meshFilter.mesh;
	if(mesh != null)
	{
	    vertices = mesh.vertices;
	    verticesColors = mesh.colors;
	    uvs = mesh.uv;
	    triangles = mesh.triangles;
	    GenerateTiles();
	}else{
	    GenerateTerrain();
	}
	singleton = this;
    }
    
    public void GenerateTerrain()
    {
	GenerateRuledHeightMap();
	GenerateVertices_new_2();
	CorrectVerticesHeight();
	GenerateUVS();
	GenerateTiles();
	GenerateTriangles();
	GenerateVertexColors();
	GenerateMesh();
	GenerateCollider();
    }

    public void GenerateVertexColors(){
	verticesColors = new Color[vertices.Length];
	for(int i = 0; i < verticesColors.Length; i++)
	{
	    verticesColors[i] = new Color(0.1f,0.1f,0.1f);
	}
    }

    public void GenerateRuleGraph(){
	connectionRules = new Dictionary<int,List<int>>();

	for(int i = 0; i <= maxHeight; i++)
	{
	    List<int> connections = new List<int>();
	    connections.Add(i);
	    connections.Add(i+1);
	    connections.Add(i+2);

	    if(i-1 >= 0){connections.Add(i-1);}

	    connectionRules.Add(i,connections);
	}
    }
    
    public void GenerateRuledHeightMap(){
	GenerateRuleGraph();

	List<int> visitedNodes = new List<int>();
	List<int> toVisitNodes = new List<int>();
	tilesHeightMap = Enumerable.Repeat(-1.0f,tileCountWidth * tileCountLenght).ToArray();

	for(int i = 0; i< tilesHeightMap.Length; i++){
	    toVisitNodes.Add(i);
	}
	
	tilesHeightMap[0] = Random.Range(0,maxHeight+1);
	tilesHeightMap[tileCountLenght-1] = Random.Range(0,maxHeight+1);
	tilesHeightMap[tileCountLenght*tileCountWidth - tileCountLenght] = Random.Range(0,maxHeight+1);
	tilesHeightMap[tileCountLenght*tileCountWidth-1] = Random.Range(0,maxHeight+1);

	visitedNodes.Add(0);
	visitedNodes.Add(tileCountLenght-1);
	visitedNodes.Add(tileCountLenght*tileCountWidth - tileCountLenght);
	visitedNodes.Add(tileCountLenght*tileCountWidth-1);

	toVisitNodes.Remove(0);
	toVisitNodes.Remove(tileCountLenght-1);
	toVisitNodes.Remove(tileCountLenght*tileCountWidth - tileCountLenght);
	toVisitNodes.Remove(tileCountLenght*tileCountWidth-1);
	
	int currentNode = toVisitNodes[Random.Range(0,toVisitNodes.Count)];

	while(toVisitNodes.Count > 0)
	{
	    int[] neighbors = GetHeightMapNeighbors(currentNode);
	    List<int> availableNeighbors = new List<int>();
	    foreach(var neighbor in neighbors)
	    {
		//if(visitedNodes.Contains(neighbor) == false){
		    availableNeighbors.Add(neighbor);
		//}
	    }
	    List<int> neighborsValues = new List<int>();

	    foreach(var neighbor in availableNeighbors)
	    {
		neighborsValues.Add((int)tilesHeightMap[neighbor]);
	    }
	    int[] availableValues = GetAvailableValues(neighborsValues.ToArray());
	    tilesHeightMap[currentNode] = availableValues.Length <= 0 ? -1 : availableValues[Random.Range(0,availableValues.Length)];

	    toVisitNodes.Remove(currentNode);
	    visitedNodes.Add(currentNode);
	    if(toVisitNodes.Count > 0)
	    {
		currentNode = toVisitNodes[Random.Range(0,toVisitNodes.Count)];
	    }
	}	
    }

    public int[] GetAvailableValues(int[] input)
    {
	List<int> availableValues = new List<int>();

	for(int i = 0 ; i <= maxHeight;i++)
	{
	    availableValues.Add(i);
	}

	foreach(var item in input)
	{
	    if(item == -1){continue;}
	    
	    List<int> tempAvailableValues = new List<int>(availableValues.ToArray());
	    
	    var connections = connectionRules[item];

	    foreach(var value in tempAvailableValues)
	    {
		if(connections.Contains(value) == false)
		{
		    availableValues.Remove(value);
		}
	    }
	}

	return availableValues.ToArray();
    }
    

    public int[] GetHeightMapNeighbors(int i){
	if(i < 0 || i >= tilesHeightMap.Length){return null;}

	int[] neighbors = new int[]{
	    i-tileCountLenght+1,i-tileCountLenght,i-tileCountLenght-1,
	    i+1,i-1,
	    i+tileCountLenght+1,i+tileCountLenght,i+tileCountLenght-1
	};
	
	List<int> validNeighbors = new List<int>();

	foreach(var neighbor in neighbors){
	    if(neighbor < 0 || neighbor >= tilesHeightMap.Length){continue;}
	    if(i%tileCountLenght==0 && (i-1 == neighbor || i+tileCountLenght-1 == neighbor || i-tileCountLenght-1 == neighbor)){continue;}
	    if((i+1)%tileCountLenght==0 && (i+1 == neighbor || i+tileCountLenght+1 == neighbor || i-tileCountLenght+1 == neighbor)){continue;}
	    
	    validNeighbors.Add(neighbor);
	}
	
	return validNeighbors.ToArray();
    }
    
    private void GenerateTiles()
    {
	tiles = new List<int[]>();
	int[] baseTile = SelectTileVertices();
	for(int tileX=0; tileX < tileCountWidth; tileX++){
	    for(int tileZ=0; tileZ < tileCountLenght; tileZ++){
		List<int> tile = new List<int>(baseTile);

		for(int vertexIndex = 0; vertexIndex < tile.Count; vertexIndex++)
		{
		    tile[vertexIndex] +=
			tileZ * tileVertexDensity +
			tileX * tileVertexDensity * tileCountLenght * tileVertexDensity;
		}
		tiles.Add(tile.ToArray());
	    }
	}
    }
    
    private int[] SelectTileVertices()
    {
	List<int> tiles = new List<int>();
	
	for(int column=0; column < tileVertexDensity; column++)
	{
	    int start = column * (tileVertexDensity * tileCountLenght);
	    for(int s = start; s < start + tileVertexDensity; s++)
	    {
		tiles.Add(s);
	    }
	}

	/*string output = "";
	foreach(var tile in tiles){
	    output+= tile.ToString() + ',';
	    }*/
	//	Debug.Log(output);
	
	return tiles.ToArray();
    } 
    
    private void GenerateVertices_new_2()
    {
	List<Vector3> vertices = new List<Vector3>();

	float offsetX = tileWidth/tileVertexDensity;
	float offsetZ = tileLenght/tileVertexDensity;
	
	float width = tileCountWidth * tileWidth;
	float length = tileCountLenght * tileLenght;

int tileX = 0,tileZ = 0;
	for(float x = 0 ; x < width ; x += offsetX)
	{
	    for(float z = 0; z < length ; z += offsetZ)
	    {
		tileX = (int)((x / width) * tileCountLenght);
		tileZ = (int)((z / length) * tileCountLenght);
		float height = tilesHeightMap[tileZ + tileX * tileCountLenght];
		float randOffsetX = Random.Range(-0.25f,0.25f) * offsetX;
		float randOffsetZ = Random.Range(-0.25f,0.25f) * offsetZ;
		var vertex = new Vector3(x + randOffsetX,height,z+randOffsetZ);
		vertices.Add(vertex);
	    }   
	}
	
	this.width = tileVertexDensity * tileCountWidth;
	this.lenght = tileVertexDensity * tileCountLenght;
	
	this.vertices = vertices.ToArray();
    }
    
    private void GenerateVertices_new()
    {
	List<Vector3> vertices = new List<Vector3>();

	List<Vector3[]> tiles = new List<Vector3[]>();
	
	for(int x = 0; x < tileCountWidth; x++){
	    for(int z = 0; z < tileCountLenght; z++){
		float height = tilesHeightMap[z + x * tileCountLenght];
		Vector3[] tile = GenerateTile(height);
		tiles.Add(tile);
	    }
	}

	for(int column = 0; column < tileVertexDensity;column++){
	    
	    bool offset = false;
	    int offsetCount = 0;
	    float offsetX = 0;
	    float offsetZ = 0;

	    for(int t = 0; t < tiles.Count; t++){
		Vector3[] tile = tiles[t];
		int start = tileVertexDensity * column;
		int end = tileVertexDensity + tileVertexDensity * column;
		
		for(int i = start; i < end; i++)
		{
		    vertices.Add(tile[i] + new Vector3(offsetX,0,offsetZ));
		}

		offset = (t+1)%tileCountLenght == 0 && t != 0;

		if(offset){
		    offsetCount+=1;
		    offsetX = offsetCount * tileWidth;
		    offsetZ = 0;
		}
		else{
		    offsetZ += tileLenght;
		    }
	    }
	}
	
	this.vertices = vertices.ToArray();
    }
   
    
    private void GenerateVertices()
    {
	List<Vector3> vertices = new List<Vector3>();
	List<Vector2> uvs = new List<Vector2>();

	for(int x = 0; x < tileCountWidth; x++){

	    for(int z = 0; z < tileCountLenght; z++){
		float height = tilesHeightMap[z + x * tileCountLenght];
		Vector3[] tile = GenerateTile(height);

		Vector2[] uv = GenerateTileUVS(tile);
		
		for(int i = 0; i < tile.Length; i++)
		{
		    tile[i] = new Vector3(tile[i].x + (tileWidth  ) * x, tile[i].y ,tile[i].z + (tileLenght ) * z);
		    vertices.Add(tile[i]);
		}
		
		for(int i = 0; i < uv.Length; i++)
		{
		    uvs.Add(uv[i]);
		}
	    }
	}
	
	this.vertices = vertices.ToArray();
	this.uvs = uvs.ToArray();
    }

    public Vector3[] GetVertexNeighbors(Vector3 vertex)
    {
	List<Vector3> v = new List<Vector3>(vertices);
	
	if(v.Contains(vertex) == false){return null;}
	
	int vertexIndex = v.IndexOf(vertex);
	
	int offset = tileCountLenght * tileVertexDensity;
	int i = vertexIndex;

	int[] verticesIndex = new int[]{
	    (i%offset==0 ? -1 : i-offset-1) , (i-offset) , ((i+1)%offset==0 ? -1 : i-offset+1),

	    (i%offset==0 ? -1 : i-1       )              , ((i+1)%offset==0 ? -1 : i+1),

	    (i%offset==0 ? -1 : i+offset-1) , (i+offset) , ((i+1)%offset==0 ? -1 : i+offset+1)};

	List<Vector3> neighbors = new List<Vector3>();
	
	foreach(var index in verticesIndex)
	{
	    if(index >= 0 && index < vertices.Length)
	    {
		neighbors.Add(vertices[index]);
	    }
	}

	return neighbors.ToArray();
    }
    
    public int[] GetVertexNeighbors(int vertexIndex)
    {
	int offset = tileCountLenght * tileVertexDensity;
	int i = vertexIndex;

	int[] verticesIndex = new int[]{
	    (i%offset==0 ? -1 : i-offset-1) , (i-offset) , ((i+1)%offset==0 ? -1 : i-offset+1),

	    (i%offset==0 ? -1 : i-1       )              , ((i+1)%offset==0 ? -1 : i+1),

	    (i%offset==0 ? -1 : i+offset-1) , (i+offset) , ((i+1)%offset==0 ? -1 : i+offset+1)};

	List<int> neighbors = new List<int>();
	
	foreach(var index in verticesIndex)
	{
	    if(index >= 0 && index < vertices.Length)
	    {
		neighbors.Add(index);
	    }
	}

	return neighbors.ToArray();
    }
    
    private void CorrectVerticesHeight(){

	float[] heights = new float[vertices.Length];
	
	for(int i = 0; i < vertices.Length; i++)
	{
	    int offset = tileCountLenght * tileVertexDensity;

	    float randomOffsetX = Random.Range(-0.05f,0.05f);
	    float randomOffsetZ = Random.Range(-0.05f,0.05f);
	    
	    int[] verticesIndex = new int[]{
		i%offset==0 ? -1 : i-offset-1 , i-offset , (i+1)%offset==0 ? -1 : i-offset+1,
		i%offset==0 ? -1 : i-1        , i        , (i+1)%offset==0 ? -1 : i+1,
		i%offset==0 ? -1 : i+offset-1 , i+offset , (i+1)%offset==0 ? -1 : i+offset+1};

	    float usableVerticesCount = 0;
	    float usableVerticesSum = 0;

	    foreach(var index in verticesIndex)
	    {
		if(index >= 0 && index < vertices.Length)
		{
		    vertices[index] = new Vector3(
			vertices[index].x + randomOffsetX,
			vertices[index].y,
			vertices[index].z + randomOffsetZ
		    );
		    
		    usableVerticesSum += vertices[index].y;
		    usableVerticesCount+=1;
		}
	    }

	    heights[i] = usableVerticesCount <= 0 ? vertices[i].y : usableVerticesSum/usableVerticesCount;
	}

	for(int i = 0; i < vertices.Length; i++)
	{
	    vertices[i] = new Vector3(vertices[i].x,heights[i],vertices[i].z);
	}
    }


    private Vector3[] GenerateTile(float height){
	List<Vector3> tile = new List<Vector3>();

	float offsetX = tileWidth/tileVertexDensity;
	float offsetZ = tileLenght/tileVertexDensity;
	
	for(float x = 0; x < tileWidth; x += offsetX){

	    for(float z = 0; z < tileLenght; z += offsetZ){
		Vector3 vertex = new Vector3(x,height,z);
		tile.Add(vertex);
	    }

	}
	
	return tile.ToArray();
    }

    private int[] GenerateTileIndices(int offset){

	List<int> triangles
	    = new List<int>();

	int w = tileVertexDensity;
	int l = tileVertexDensity;
	
	for (int x = 0; x < w-1; x++)
        {
            for (int z = 0; z < l-1; z++)
            {
                triangles.Add(x * w + z + offset);
                triangles.Add(x * w + z + 1 + offset);
                triangles.Add(x * w + l + z + offset);

                triangles.Add(x * w + l + z + offset);
                triangles.Add(x * w + z + 1 + offset);
                triangles.Add(x * w + l + z + 1 + offset);
            }
	}

	return triangles.ToArray();
    }

    public Vector2[] GenerateTileUVS(Vector3[] vertices)
    {
	float width = tileVertexDensity;
	float length = tileVertexDensity;
	
	Vector2[] uvs = new Vector2[vertices.Length];
	for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x/width, vertices[i].z/length);
        }

	return uvs;
    }
    
    public void GenerateUVS()
    {
	uvs = new Vector2[vertices.Length];
	for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x/width, vertices[i].z/lenght);
        }

    }
    private void GenerateTriangles()
    {
	List<int> triangles
	    = new List<int>();

	int w = width;
	int l = lenght;
		
	for (int x = 0; x < w-1; x++)
        {
            for (int z = 0; z < l-1; z++)
            {
		triangles.Add(x * l +  z);
		triangles.Add(x * l + z + 1);
		triangles.Add(x * l + l + z);

		triangles.Add(x * l + z + 1);
		triangles.Add(x * l + l + z + 1);
		triangles.Add(x * l + l + z);
	    }
	this.triangles = triangles.ToArray();
    }
    }
    
    /*
    private void GenerateIndices()
    {
	List<int> indices = new List<int>();
	
	int offset = 0;
	int offsetCount = tileVertexDensity * tileVertexDensity;
	for(int x = 0; x < tileCountWidth; x++){

	    for(int z = 0; z < tileCountLenght; z++){
		int[] tileIndices = GenerateTileIndices(offset);

		foreach(var item in tileIndices){
		    indices.Add(item);
		}
		
		offset += offsetCount;
	    }
	}
	for(int x = 0; x < tileCountWidth; x++){
	    
	    for(int z = 0; z < tileCountLenght; z++)
	    {
		if(z < tileCountLenght-1){
		List<int> topPoints = new List<int>();
		List<int> topPoints_other = new List<int>();
		int valueTop = (tileVertexDensity-1) + tileVertexDensity*tileVertexDensity * z + (tileVertexDensity*tileVertexDensity * tileCountLenght * x);

		for(int i = 0 ; i < tileVertexDensity; i++)
		{
		    int valueOther = valueTop + (tileVertexDensity*tileVertexDensity - (tileVertexDensity-1));
		    topPoints.Add(valueTop);
		    topPoints_other.Add(valueOther);
		    valueTop += tileVertexDensity;
		}

		for(int i = 0; i < topPoints.Count-1; i++){
		    indices.Add(topPoints[i]);
		    indices.Add(topPoints_other[i]);
		    indices.Add(topPoints[i+1]);

		    indices.Add(topPoints[i+1]);
		    indices.Add(topPoints_other[i]);
		    indices.Add(topPoints_other[i+1]);
		}
		}

		if(x < tileCountWidth - 1){
		List<int> rightPoints = new List<int>();
		List<int> rightPoints_other = new List<int>();
		
		int valueRight = ((tileVertexDensity*tileVertexDensity) - tileVertexDensity) + tileVertexDensity*tileVertexDensity * z + (tileVertexDensity*tileVertexDensity * tileCountLenght * 2 * x);

		for(int i = 0 ; i < tileVertexDensity; i++){
		    int valueOther = valueRight + (tileVertexDensity*tileVertexDensity + tileVertexDensity);
		    rightPoints.Add(valueRight);
		    rightPoints_other.Add(valueOther);
		    Debug.Log("Value Right:" + valueRight.ToString());
		    Debug.Log("Value Other:" + valueOther.ToString());
		    valueRight += 1;
		}
		Debug.Log("--- --- ---");	
		for(int i = 0; i < rightPoints.Count-1; i++){
		    indices.Add(rightPoints[i]);
		    indices.Add(rightPoints[i+1]);
		    indices.Add(rightPoints_other[i]);
		    
		    indices.Add(rightPoints[i+1]);
		    indices.Add(rightPoints_other[i+1]);
		    indices.Add(rightPoints_other[i]);
		}
		}
	    }
	}
	
	this.indices = indices.ToArray();
    }
    */
    
    private void GenerateMesh()
    {
	meshFilter.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        meshFilter.mesh.vertices = vertices;
        meshFilter.mesh.triangles = triangles;
	meshFilter.mesh.uv = uvs;
	meshFilter.mesh.colors = verticesColors;
	
        meshFilter.mesh.RecalculateNormals();

	meshFilter.sharedMesh = meshFilter.mesh;
    }

    public void UpdateMeshVerticesColors(){
	meshFilter.mesh.colors = verticesColors;
    }

    private void GenerateCollider(){
	MeshCollider collider = GetComponent<MeshCollider>();
	collider.sharedMesh = meshFilter.mesh;
    }

    private void OnDrawGizmos()
    {
	if(debug == false){return;}

	if(tiles != null){
	    foreach(var tile in tiles){
	    
		Color randomColor = new Color(
		    Random.Range(0.0f,1.0f),
		    Random.Range(0.0f,1.0f),
		    Random.Range(0.0f,1.0f)
		);
		foreach(var index in tile){
		    Gizmos.color = randomColor;
		    Gizmos.DrawSphere(vertices[index],0.5f);   
		}
	    }
	}
	/*	
	if(vertices != null)
	{
	    int i = 0;
	    int tile = 0;
	    foreach(var item in vertices)
	    {

		Gizmos.color = new Color(0,1/(item.y + 0.0001f),0);
		Gizmos.DrawSphere(item,0.1f);
		Handles.Label(item, i.ToString());
		i+=1;
	    }
	    }*/
    }
    
}
