using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphNode
{
    public Vector3 vertex;
    public int[] neighbors;

    public bool walkable;

    public GraphNode(Vector3 vertex,int[] neighbors,bool walkable){
	this.vertex = vertex;
	this.neighbors = neighbors;
	this.walkable = walkable;
    }
}

[RequireComponent(typeof(TerrainGenerator))]
public class TerrainGeneratorNavMesh : MonoBehaviour
{
    [Range(0,1)]
    public float maxWalkableInclineThreshold = 0.9f;

    private Vector3[] walkableVertices;
    private Vector3[] unwalkableVertices;
    
    public Vector3[] WalkableVertices => walkableVertices;
    public Vector3[] UnwalkableVertices => unwalkableVertices;

    public GraphNode[] graph;
    public GraphNode[] Graph => graph;
    
    public LayerMask terrainLayer;

    public TerrainGenerator terrainGenerator;

    [Min(0)]
    public float maxWalkableVertexHeight;
    [Min(0)]
    public float maxWalkableAgentHeight;
    public float maxWalkableVertexRadius;
    
    public Vector3 upVector;
    
    public bool debug;

    public static TerrainGeneratorNavMesh singleton;
    
    public void GenerateWalkableVertices()
    {
	if(singleton != null){Destroy(this); return;}
	
	List<Vector3> verticesW = new List<Vector3>();
	List<Vector3> verticesU = new List<Vector3>();

	MeshFilter mf = GetComponent<MeshFilter>();
	Mesh mesh = mf.mesh;
	Vector3[] normals = mesh.normals;
	Vector3[] vertices = mesh.vertices;

	int vertex = 0;
	foreach(var normal in normals){
	    if(IsWalkableVertex(vertices[vertex],normal))
	    {
		Debug.Log("ADD WALKABLE VERTEX");
		verticesW.Add(vertices[vertex]);
	    }
	    else{
		verticesU.Add(vertices[vertex]);
	    }
	    vertex+=1;
	}

	walkableVertices = verticesW.ToArray();
	unwalkableVertices = verticesU.ToArray();

	GenerateGraph();
	singleton = this;
    }

    private void GenerateGraph()
    {
	terrainGenerator = GetComponent<TerrainGenerator>();

	List<GraphNode> graph = new List<GraphNode>();
	
	MeshFilter mf = GetComponent<MeshFilter>();
	Mesh mesh = mf.mesh;
	Vector3[] vertices = mesh.vertices;

	foreach(var vertice in vertices)
	{
	    bool walkable = IsWalkableVertex(vertice);
	    GraphNode node = new GraphNode(vertice,null,walkable);
	    graph.Add(node);
	}

	int i = 0;
	foreach(var node in graph)
	{
	    node.neighbors = terrainGenerator.GetVertexNeighbors(i);
	    i++;
	}

	this.graph = graph.ToArray();
    }

    public GraphNode[] GetGraphNeighbors(GraphNode node)
    {
	List<GraphNode> nodes = new List<GraphNode>();

	foreach(var neighbor in node.neighbors)
	{
	    nodes.Add(graph[neighbor]);
	}

	return nodes.ToArray();
    }

    /*private void GenerateNeighboredWalkableVertices()
    {
	terrainGenerator = GetComponent<TerrainGenerator>();
	List<NeighboredVertex> neighboredVertices = new List<NeighboredVertex>();
	
	MeshFilter mf = GetComponent<MeshFilter>();
	Mesh mesh = mf.sharedMesh;
	Vector3[] normals = mesh.normals;
	Vector3[] vertices = mesh.vertices;

	for(int v = 0; v < vertices.Length;v++){
	    if(IsWalkableVertex(vertices[v]) == false){continue;}
	    
	    NeighboredVertex nVertex = new NeighboredVertex();
	    nVertex.vertex = vertices[v];
	    
	    int[] neighbors = terrainGenerator.GetVertexNeighbors(v);
	    List<Vector3> newNeighbors = new List<Vector3>();
	    
	    foreach(var neighbor in neighbors){
		Vector3 vertex = vertices[neighbor];
		if(IsWalkableVertex(vertex))
		{
		    newNeighbors.Add(vertex);
		}
	    }
	    nVertex.neighbors = newNeighbors.ToArray();
	    neighboredVertices.Add(nVertex);
	}
	walkableNeighboredVertices = neighboredVertices.ToArray();
    }*/

    public bool IsWalkableVertex(Vector3 vertex, Vector3 vertexNormal){
	return Vector3.Dot(vertexNormal,upVector) >= maxWalkableInclineThreshold
	    //&& Physics.Raycast(vertex,vertexNormal,maxWalkableAgentHeight,~terrainLayer) == false
	    //&& Physics.OverlapSphere(vertex,maxWalkableVertexRadius,~terrainLayer).Length == 0
	    && vertex.y <= maxWalkableVertexHeight;
    }

    public bool IsWalkableVertex(Vector3 vertex)
    {
	List<Vector3> vertices = new List<Vector3>(walkableVertices);
	return vertices.Contains(vertex);
    }

    private void OnDrawGizmos()
    {
	if(debug)
	{	    
	    if(unwalkableVertices != null)
	    {
		Gizmos.color = Color.red;
		foreach(var vertex in unwalkableVertices)
		{
		    Gizmos.DrawSphere(vertex,0.25f);
		}
	    }
	    
	    if(walkableVertices != null)
	    {
		Gizmos.color = Color.green;
		foreach(var vertex in walkableVertices)
		{
		    Gizmos.DrawSphere(vertex,0.25f);
		}
	    }
	}
    }
}
    
