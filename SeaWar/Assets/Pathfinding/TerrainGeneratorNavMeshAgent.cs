using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode
{
    public float gCost;
    public float hCost;
    public float fCost;

    public PathNode comeFromNode;

    public GraphNode graphNode;
    
    public void CalculateFCost(){
	fCost = gCost + hCost;
    }

    public static bool IsSame(PathNode a,PathNode b){
	return a.graphNode.vertex == b.graphNode.vertex;
    }
}

public class TerrainGeneratorNavMeshAgent : MonoBehaviour
{
    [Range(0.0f,1.0f)]
    public float offsetTimeBuffer;
    public bool check = true;

    [SerializeField]
    public TerrainGeneratorNavMesh navMeshReference;

    public int currentNode;
    public int targetNode;
    public bool noPathFound = false;
    
    private const int MOVE_STRAIGHT_COST = 10;
    private const int MOVE_DIAGONAL_COST = 14;

    private List<PathNode> pathNodeGraph;


    public Vector3 lastTarget;
    public Vector3 target;

    public Vector3 targetNormal;

    public bool debug;

    public float moveSpeed;
    
    public List<Vector3> walkPath;
    public bool move;

    public void Start(){
	ResetAgent();
    }
    
    public void Update()
    {
	if(move)
	{
	    Move();
	}
    }

    public void Move(){
	targetNode = pathNodeGraph.Count-1;
	if(new Vector2(lastTarget.x,lastTarget.z) != new Vector2(target.x,target.z))
	{
	    ResetAgent();
	    SetPathToTarget();
	    lastTarget = target;
	}
	else{noPathFound = false;}
	MoveThroughPath();
    }

    private void MoveThroughPath()
    {
	if(walkPath == null){
	    move = false; return;}
	
	if(walkPath.Count <= 0){
	    move = false; return;}
	
	Vector3 forward = new Vector3(
	    walkPath[0].x-transform.position.x,
	    0,
	    walkPath[0].z-transform.position.z
	);

	transform.forward = forward;
	
	transform.position = Vector3.MoveTowards(
	    transform.position,
	    walkPath[0],
	    moveSpeed*Time.fixedDeltaTime);	

	if(Vector3.Distance(transform.position,walkPath[0]) < 0.01f)
	{
	    walkPath.RemoveAt(0);
	}
    }

    private int GetClosestNodeIndex()
    {
	Dictionary<int,float> distances = new Dictionary<int,float>();
	
	int index = 0;
	foreach(var graphNode in navMeshReference.Graph)
	{
	    if(graphNode.walkable == false){index+=1;continue;}

	    distances.Add(
		index,
		Vector2.Distance(
		    new Vector2(transform.position.x,transform.position.z),
		    new Vector2(graphNode.vertex.x,graphNode.vertex.z)));

	    index+=1;
	}

	var sortedDistances = distances.OrderBy(x => x.Value);
	return sortedDistances.ElementAt(0).Key;
    }
    
    public void ResetAgent()
    {
	PositionTargetOnTerrain();
	AddTargetToGraph();
	GeneratePathNodeGraph();
    }

    private void GeneratePathNodeGraph()
    {
	if(pathNodeGraph != null){pathNodeGraph.Clear();}else{pathNodeGraph = new List<PathNode>();}

	foreach(var graphNode in navMeshReference.Graph)
	{
	    PathNode pathNode = new PathNode();
	    pathNode.graphNode = graphNode;
	    
	    pathNode.comeFromNode = null;
	    pathNodeGraph.Add(pathNode);
	}
    } 

    public void SetPathToTarget()
    {
	currentNode = GetClosestNodeIndex();
	walkPath = GetPathToTarget();
    }

    private List<Vector3> GetPathToTarget()
    {
	if(navMeshReference.IsWalkableVertex(target,targetNormal) == false){noPathFound = true; return null;}
	
	Debug.Log("HELLO");
	PathNode startNode = pathNodeGraph[currentNode];
	PathNode endNode = pathNodeGraph[targetNode];
	
	List<PathNode> openList = new List<PathNode>();
	List<PathNode> closedList = new List<PathNode>();

	//int firstTargetNeighbor;
	
	foreach(var node in pathNodeGraph){
	    node.gCost = int.MaxValue;
	    node.CalculateFCost();
	    node.comeFromNode = null;
	}

	startNode.gCost = 0;
	startNode.hCost = CalculateDistanceCost(startNode,endNode);
	startNode.CalculateFCost();

	openList.Add(startNode);
	
	while(openList.Count > 0)
	{
	    PathNode currentNode = GetLowestFCostNode(openList);
	    if(PathNode.IsSame(currentNode, endNode))
	    {
		noPathFound = false;
		return CalculatePath(endNode);
	    }

	    openList.Remove(currentNode);
	    closedList.Add(currentNode);

	    foreach(var node in GetPathNodeNeighbors(currentNode.graphNode.neighbors))
	    {
		if(ContainsInList(closedList,node)) continue;
		if(node.graphNode.walkable == false){
		    closedList.Add(node);
		    continue;
		}
		
		float tentativeGCost = currentNode.gCost + CalculateDistanceCost(currentNode,node);
		if(tentativeGCost < node.gCost){
		    node.comeFromNode = currentNode;
		    node.gCost = tentativeGCost;
		    node.hCost = CalculateDistanceCost(node,endNode);
		    node.CalculateFCost();

		    if(ContainsInList(openList,node) == false){
			openList.Add(node);
		    }
		}
	    }
	}
	Debug.Log("NO PATH FOUND");
	noPathFound = true;
	return null;
    }

    private bool ContainsInList(List<PathNode> list, PathNode node){
	foreach(var item in list){
	    if(PathNode.IsSame(item,node)){
		return true;
	    }
	}
	return false;
    }
    
    private PathNode[] GetPathNodeNeighbors(int[] neighborsIndexes)
    {
	List<PathNode> neighbors = new List<PathNode>();
	
	foreach(var index in neighborsIndexes)
	{
	    neighbors.Add(pathNodeGraph[index]);
	}
	return neighbors.ToArray();
    }

    private List<Vector3> CalculatePath(PathNode node){
	List<Vector3> path = new List<Vector3>();
	path.Add(node.graphNode.vertex);
	PathNode currentNode = node;
	while(currentNode.comeFromNode != null)
	{
	    path.Add(currentNode.graphNode.vertex);
	    currentNode = currentNode.comeFromNode;
	}
	path.Reverse();
	return path;
    }
    
    private float CalculateDistanceCost(PathNode a, PathNode b)
    {
	float xDistance = Mathf.Abs(a.graphNode.vertex.x - b.graphNode.vertex.x);
	float zDistance = Mathf.Abs(a.graphNode.vertex.z - b.graphNode.vertex.z);
	float remaining = Mathf.Abs(xDistance - zDistance);

	return MOVE_DIAGONAL_COST * Mathf.Min(xDistance,zDistance) + MOVE_STRAIGHT_COST * remaining;
    }

    private PathNode GetLowestFCostNode(List<PathNode> pathNodeList)
    {
	PathNode lowestFCostNode = pathNodeList[0];
	for(int i = 1; i<pathNodeList.Count; i++)
	{
	    if(pathNodeList[i].fCost < lowestFCostNode.fCost){
		lowestFCostNode = pathNodeList[i];
	    }
	}

	return lowestFCostNode;
    }
    
    private void PositionTargetOnTerrain()
    {
	RaycastHit hit;
	if(Physics.Raycast(target+new Vector3(0,100,0), new Vector3(0,-1,0),out hit ,Mathf.Infinity,navMeshReference.terrainLayer)){
	    target = hit.point;
	    targetNormal = hit.normal;
	}
    }

    private void AddTargetToGraph(){
	AddTargetToGraphAsNode();
	AddTargetToGraphAsNeighbor();
    }
    
    private void AddTargetToGraphAsNode(){
	List<GraphNode> graph = new List<GraphNode>(navMeshReference.graph);
	GraphNode node = new GraphNode(target,null,navMeshReference.IsWalkableVertex(target,targetNormal));
	if(graph.Count < (navMeshReference.terrainGenerator.Width * navMeshReference.terrainGenerator.Lenght)+1)
	{
	    graph.Add(node);
	    navMeshReference.graph = graph.ToArray();
	}else
	{
	    graph[navMeshReference.terrainGenerator.Width * navMeshReference.terrainGenerator.Lenght] = node;	    	    
	    navMeshReference.graph = graph.ToArray();
	}
    }

    private void AddTargetToGraphAsNeighbor()
    {
	int targetIndex = navMeshReference.Graph.Length-1;
	Dictionary<int,float> distances = new Dictionary<int,float>();
    
	int index = 0;
	foreach(var graphNode in navMeshReference.Graph)
	{
	    distances.Add(index,Vector2.Distance(new Vector2(target.x,target.z),new Vector2(graphNode.vertex.x,graphNode.vertex.z)));
	    index+=1;
	}

	var sortedDistances = distances.OrderBy(x => x.Value);
	int firstNDistances = distances.Count < 4 ? distances.Count : 4;

	foreach(var node in navMeshReference.graph){
	    if(node.neighbors == null){continue;}
	    List<int> tempNeighbors = new List<int>(node.neighbors);
	    if(tempNeighbors.Contains(navMeshReference.graph.Length-1))
	    {
		tempNeighbors.Remove(navMeshReference.graph.Length-1);
		node.neighbors = tempNeighbors.ToArray();
	    }
	}
	
	for(int count = 0; count < firstNDistances ; count++)
	{
	    index = sortedDistances.ElementAt(count).Key;
	    
	    var tempNeighbors = navMeshReference.graph[index].neighbors;
	    List<int> lastNeighbors = tempNeighbors == null ? new List<int>() : new List<int>(navMeshReference.graph[index].neighbors);
	    lastNeighbors.Add(targetIndex);
	    lastNeighbors = lastNeighbors.Distinct().ToList();
	    navMeshReference.graph[index].neighbors = lastNeighbors.ToArray();

	    tempNeighbors = navMeshReference.graph[targetIndex].neighbors;
	    lastNeighbors = tempNeighbors == null ? new List<int>() : new List<int>(navMeshReference.graph[targetIndex].neighbors);
	    lastNeighbors.Add(index);
	    lastNeighbors = lastNeighbors.Distinct().ToList();
	    navMeshReference.graph[targetIndex].neighbors = lastNeighbors.ToArray();
	}
    }

    private bool AllTargetNeighborAreWalkable()
    {
	foreach(var neighbor in navMeshReference.graph[navMeshReference.graph.Length-1].neighbors)
	{
	    if(navMeshReference.graph[neighbor].walkable == false){
		return false;}	    	   
	}
	return true;
    }
    
    private void OnDrawGizmos()
    {
	if(debug)
	{	    
	    if(walkPath != null){

		foreach(var item in walkPath){
		    Gizmos.color = Color.blue;
		    Gizmos.DrawSphere(item,0.5f);
		}

		Gizmos.color = Color.green;
		Gizmos.DrawSphere(target,0.5f);
	    }
	}
    }
}
