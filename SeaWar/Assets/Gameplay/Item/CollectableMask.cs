using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableMask : MonoBehaviour
{
    public MeshRenderer[] mrs;
    
    private void Start(){
	EnableMask();
    }
    
    private void OnTriggerEnter(Collider other){
	if(other.tag == "Pawn" || other.tag == "Enemy")
	{
	    DisableMask(); 
	}
    }

    private void EnableMask(){
	foreach (var renderer in mrs){
	    renderer.enabled = false;
	}
    }

    private void DisableMask(){
	foreach (var renderer in mrs){
	    renderer.enabled = true;
	}
    }
}
