using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CollectableType{
    Rock,
    EnergyRock,
    Vidranium,
    Coral
}

public class CollectableManager : MonoBehaviour
{
    [Range(1,5)]
    public int collectPoints;

    public CollectableType type;
    
    public void TakeCollectPoints(int value){
	collectPoints = collectPoints - value < 0 ? 0 : collectPoints - value;
    }

    public void Update()
    {
	if(collectPoints <= 0)
	{
	    Destroy(gameObject);
	}
    }
    
}
