using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionManager : MonoBehaviour
{
    public List<Action> actions;
    public int actionsCount;
    public string currentActionsTag;

    public bool mine = false;
    public bool build = false;
    
    void Start(){actions = new List<Action>();}

    public void DoActions(AnimationManager animationManger){
	if(actions == null) {return;}
	actionsCount = actions.Count;
	if(actions.Count > 0){
	    var action = actions[0];
	    currentActionsTag = actions[0].actionType;

	    /*if(Broadcaster.GetBroadcastMessage()
	       == action.stopBroadcastMessage){
		    animationManger.SetMove(false);
		actions.RemoveAt(0);
		return;
		}*/
	    
	    var agent = GetComponent<TerrainGeneratorNavMeshAgent>();
	    switch(action.actionType){
		case "Terrain":
		    animationManger.SetMove(true);
		    agent.target = action.targetPosition;
		    agent.move = true;
		    if(Vector3.Distance(transform.position, agent.target) <= 0.01f)
		    {
			//Broadcaster.BroadcastMessage(action.stopBroadcastMessage);
		    animationManger.SetMove(false);
			actions.RemoveAt(0);
		    }
		    break;
		case "Collectable":
		    agent.target = action.targetPosition;
		    agent.move = true;

		    if(Vector3.Distance(transform.position, agent.target) <= 0.8f){
			agent.move = false;
			animationManger.SetMine(true);
			transform.LookAt(agent.target);
			if(mine == false){
			    StartCoroutine(Mine(animationManger));
			}
		    }
		    else{
			animationManger.SetMove(true);
		    }
		    
		    break;
		case "Pawn":
		    //Broadcaster.BroadcastMessage(action.stopBroadcastMessage);
		    actions.RemoveAt(0);
		    animationManger.SetMove(false);
		    break;
		case "Enemy":
		    if(actions.Count > 1){
			if(actions[1].actionType != "Enemy"){
			    actions.RemoveAt(0);
		    animationManger.SetMove(false);
			    //Broadcaster.BroadcastMessage(action.stopBroadcastMessage);
			}
		    }

		    if(action.target == null)
		    {
			transform.GetChild(0).gameObject.GetComponent<Collider>().enabled = false;
			animationManger.SetMove(false);
			actions.RemoveAt(0);
			//Broadcaster.BroadcastMessage(action.stopBroadcastMessage);
		    }
		    
		    agent.target = action.targetPosition;
		    agent.move = true;
		    if(Vector3.Distance(transform.position, agent.target) <= 0.8f){
			if(Vector3.Distance(transform.position, action.target.transform.position) > 1f){
		    animationManger.SetMove(false);
			    actions.RemoveAt(0);
			    //Broadcaster.BroadcastMessage(action.stopBroadcastMessage);
			}
			agent.move = false;
			animationManger.SetAttack(true);
			transform.LookAt(agent.target);
		    }
		    else{
			animationManger.SetMove(true);
		    }
		    
		    break;
		case "Building":
		    agent.target = action.targetPosition;
		    agent.move = true;

		    if(Vector3.Distance(transform.position, agent.target) <= 1.0f){
			agent.move = false;
			animationManger.SetMine(true);
			transform.LookAt(agent.target);
			if(build == false){
			    Debug.Log("aoeu");
			    StartCoroutine(Build(animationManger));
			}
		    }
		    else{
			animationManger.SetMove(true);
		    }
		    break;
	    }
	    
	}
    }

    private IEnumerator Build(AnimationManager animationManger){
	build = true;
	yield return new WaitForSeconds(3);

	var action = actions[0];
	var targetObject = action.target; 
	var buildingManager = targetObject.GetComponent<BuildingManager>();

	buildingManager.buildPoints += 1;
	build= buildingManager.isReady;

	if(buildingManager.buildPoints >= buildingManager.requiredReadyPoints){
	    build = false;
	    actions.RemoveAt(0);

			animationManger.SetMine(false);
		    animationManger.SetMove(false);
	    //Broadcaster.BroadcastMessage(action.stopBroadcastMessage);
	}
    }

    private IEnumerator Mine(AnimationManager animationManger){
	mine = true;
	yield return new WaitForSeconds(3);

	var action = actions[0];
	var targetObject = action.target; 
	var collectableManager = targetObject.GetComponent<CollectableManager>();

	collectableManager.TakeCollectPoints(1);
	var pawn  = GetComponent<Pawn>();
	var baseManager = pawn.baseManager;

	switch(collectableManager.type)
	{
	    case CollectableType.Rock:
		baseManager.rockCount+=1;
		break;
	    case CollectableType.EnergyRock:
		baseManager.energyRockCount+=1;
		break;
	    case CollectableType.Vidranium:
		baseManager.vidraniumCount+=1;
		break;
	    case CollectableType.Coral:
		baseManager.coralCount+=1;
		break;
	}
	
	mine = !(collectableManager.collectPoints > 0);
	if(collectableManager.collectPoints <= 0){
	    mine = false;
	    actions.RemoveAt(0);

			animationManger.SetMine(false);
		    animationManger.SetMove(false);
	    //Broadcaster.BroadcastMessage(action.stopBroadcastMessage);
	}
    }

    public void AddAction(string stopBroadcastMessage,string actionType,
			  GameObject target,
			  Vector3 targetPosition){
	actions.Add(new Action(
			stopBroadcastMessage,
			actionType,
			target,
			targetPosition));
    }

    public void AddAction(Action action){
	actions.Add(action);
    }
}
