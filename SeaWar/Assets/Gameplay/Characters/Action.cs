using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action
{
    public string stopBroadcastMessage;
    public string actionType;
    
    public GameObject target;
    public Vector3 targetPosition;

    public Action(Action action){
	stopBroadcastMessage = action.stopBroadcastMessage;
	target = action.target;
	actionType = action.actionType;
	targetPosition = action.targetPosition;
    }
    
    public Action(string stopBroadcastMessage,
		  string actionType,
		  GameObject target,
		  Vector3 targetPosition){
	this.stopBroadcastMessage = stopBroadcastMessage;
	this.target = target;
	this.targetPosition = targetPosition;
	this.actionType = actionType;
    }
}
