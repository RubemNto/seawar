using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerChecker : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
	if(other.gameObject.tag == "Enemy")
	{
	    other.gameObject.GetComponent<Pawn>().TakeHealth(1); 
	}
    }
}
