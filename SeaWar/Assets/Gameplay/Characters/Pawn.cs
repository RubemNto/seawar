using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PawnType
{
    Pawn,
    Attacker,
    Builder,
    Explorer
}

public class Pawn : MonoBehaviour
{
    public PawnType type; 

    private Attacker attacker;
    private Builder builder;
    private Explorer explorer;

    public TerrainGeneratorNavMeshAgent navMeshAgent;

    public bool listening;
    public ActionManager actionManager;
    public AnimationManager animationManger;

    public BaseManager baseManager;

    public int healthPoints;
    
    public void Start()
    {
	listening = false;
	navMeshAgent.navMeshReference = TerrainGeneratorNavMesh.singleton;
	navMeshAgent.ResetAgent();
    }

    public void Awake()
    {
	attacker = GetComponent<Attacker>();
	builder = GetComponent<Builder>();
	explorer = GetComponent<Explorer>();

	navMeshAgent = GetComponent<TerrainGeneratorNavMeshAgent>();
	SetPawnType(type);
    }    


    public void Update(){
	actionManager.DoActions(animationManger);

	if(healthPoints <=0)
	{
	    Destroy(gameObject);
	}
    }

    public void TakeHealth(int value){
	healthPoints= healthPoints - value < 0 ? 0 : healthPoints - value;
    }
    
    public void SetPawnType(PawnType pawnType)
    {
	type = pawnType;

	switch(type){
	    case PawnType.Pawn:
		attacker.enabled = true;
		builder.enabled = true;
		explorer.enabled = true;
		break;
	    case PawnType.Attacker:
		attacker.enabled = true;
		builder.enabled = false;
		explorer.enabled = false;
		break;
	    case PawnType.Builder:
		attacker.enabled = false;
		builder.enabled = true;
		explorer.enabled = false;
		break;
	    case PawnType.Explorer:
		attacker.enabled = false;
		builder.enabled = false;
		explorer.enabled = true;
		break;
	}
    }
}
