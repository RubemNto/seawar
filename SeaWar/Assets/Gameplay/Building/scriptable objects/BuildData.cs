using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuildData : MonoBehaviour
{
    public GameObject buildModel;
    
    public string title;
    public string description;

    public Sprite icon;
    
    public Sprite[] items;
    public int[] itemsCount;

    public GameObject panel;
    
    public void ToggleData(){
	var titlepanel = panel.transform.GetChild(0).GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();
	var descriptionpanel = panel.transform.GetChild(0).GetChild(1).gameObject.GetComponent<TextMeshProUGUI>();
	var iconpanel = panel.transform.GetChild(0).GetChild(4).gameObject.GetComponent<Image>();

	titlepanel.text = title;
	descriptionpanel.text = description;
	iconpanel.sprite = icon;

	var costpanel = panel.transform.GetChild(0).GetChild(2);
	var costItem1 = costpanel.GetChild(0);
	var costItem2 = costpanel.GetChild(1);
	var costItem3 = costpanel.GetChild(2);

	costItem1.GetChild(0).gameObject.GetComponent<Image>().sprite = items[0];
	costItem1.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = itemsCount[0].ToString();
	
	costItem2.GetChild(0).gameObject.GetComponent<Image>().sprite = items[1];
	costItem2.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = itemsCount[1].ToString();
	
	costItem3.GetChild(0).gameObject.GetComponent<Image>().sprite = items[2];
	costItem3.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = itemsCount[1].ToString();

	var buildPanelContentManager = panel.transform.GetChild(0).gameObject.GetComponent<ContentPanelManagerBuild>();
	buildPanelContentManager.buildData = this;
	
	panel.SetActive(!panel.activeSelf);
    }
   
}
