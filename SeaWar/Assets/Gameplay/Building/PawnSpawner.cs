using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnSpawner : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] spawnablePrefabs;

    public int startSpawnCount;
    public int startSpawnablePrefab;

    public BaseManager baseManager;
    
    private void Start()
    {
	for(int i = 0; i < spawnPoints.Length; i++)
	{
	    if(startSpawnCount < 0){break;}
	    Vector3 point = spawnPoints[i].position;
	    Quaternion rotation = spawnPoints[i].rotation;
	    GameObject instance = Instantiate(spawnablePrefabs[startSpawnablePrefab],point,rotation) as GameObject;

	    instance.GetComponent<Pawn>().baseManager = baseManager;
	    baseManager.instancedPawns.Add(instance);
	    
	    startSpawnCount-=1;
	}
    }
    
    
    public void spawnPrefabAt(int prefabIndex,int pointIndex){
	Instantiate(spawnablePrefabs[prefabIndex],
		    spawnPoints[pointIndex].position,
		    spawnPoints[pointIndex].rotation);
    }
    
}
