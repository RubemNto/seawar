using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    public float buildPoints;
    public float requiredReadyPoints;
    
    public bool isReady;

    public Material[] originals;
    
    public Material wireframe;
    public MeshRenderer meshRenderer;
    public Collider col;
    
    private void Start(){
	originals = meshRenderer.materials;
	col = GetComponent<SphereCollider>();
	SetRenderMaterials();
    }

    void Update()
    {
	if(isReady == false)
	{
	    isReady = requiredReadyPoints <= buildPoints;
	    SetRenderMaterials();
	}
    }
    
    public void SetRenderMaterials(){
	var mats = new Material[meshRenderer.materials.Length];
	for (int i = 0 ; i < mats.Length; i++){
	    mats[i] = isReady ? originals[i] : wireframe;
	}
	meshRenderer.materials = mats;
    }

    public void EnableCollider()
    {
	col.enabled = true;
    }
    
}
