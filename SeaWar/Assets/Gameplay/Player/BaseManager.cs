using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseManager : MonoBehaviour
{
    [SerializeField]
    public List<GameObject> instancedPawns;

    [SerializeField]
    public List<GameObject> activePawns;

    public int rockCount;
    public int energyRockCount;
    public int vidraniumCount;
    public int coralCount;
    public int foodCount;

    public bool inCraftState;

    public void ToggleCraftState(){
	inCraftState = !inCraftState;
    }    
}
