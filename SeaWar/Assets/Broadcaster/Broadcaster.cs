using UnityEngine;

public static class Broadcaster
{
    private static string broadcast_message = "";

    public static void BroadcastMessage(string message){
	if(message == null){return;}

	Debug.Log("Broadcast: " + message);

	broadcast_message = message;
    }

    public static string GetBroadcastMessage(){
	string message = broadcast_message;
	broadcast_message = "";
	return message;
    }
}
